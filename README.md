reactjs tutorial
------

source: https://facebook.github.io/react/tutorial/tutorial.html

to run:
------
```bash
docker run -dti --name reactdev -p 3000:3000 -p 19000:19000 -v $PWD:/app node:6 bash
```

native
-------
Success! Created my-native-app at /home/codio/workspace/my-native-app
Inside that directory, you can run several commands:

  `npm start`
    Starts the development server so you can open your app in the Expo
    app on your phone.

  `npm run ios`
    (Mac only, requires Xcode)
    Starts the development server and loads your app in an iOS simulator.

  `npm run android`
    (Requires Android build tools)
    Starts the development server and loads your app on a connected Android
    device or emulator.

  `npm test`
    Starts the test runner.

  `npm run eject`
    Removes this tool and copies build dependencies, configuration files
    and scripts into the app directory. If you do this, you can’t go back!

notes
-------
